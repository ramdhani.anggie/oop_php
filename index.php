<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');

    $sheep = new animal ("Shaun");
    echo "Name : " . $sheep->name . "<br>" ; // "shaun"
    echo "legs : " . $sheep->legs . "<br>" ; // 4
    echo "Cold Blooded : " . $sheep->cold_blooded . "<br>" ; // "no"
    echo  "<br>";

    $kodok = new frog ("Buduk");
    echo "Name : " . $kodok->name . "<br>" ; // "Buduk"
    echo "legs : " . $kodok->legs . "<br>" ; // 4
    echo "Cold Blooded : " . $kodok->cold_blooded . "<br>" ; // "no"
    $kodok->jump("Hop Hop");
    echo  "<br>";

    $sungokong = new ape ("Kera Sakti");
    echo "Name : " . $sungokong->name . "<br>" ; // "kera sakti"
    echo "legs : " . $sungokong->legs . "<br>" ; // 2
    echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>" ; // "no"
    $sungokong->yell("Auooo");
    echo "<br>";

?>